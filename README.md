# Criar um aplicativo de consulta a API do [The Movie Database](https://developers.themoviedb.org/3/getting-started/introduction)#

Criar um aplicativo para consultar a [API do The Movie DB](https://developers.themoviedb.org/3/getting-started/introduction) e trazer os [filmes populares](https://developers.themoviedb.org/3/movies/get-popular-movies). Basear-se no mockup fornecido:

![](images/desafio.png)

### **Deve conter** ###

- __Lista de filmes populares__. Exemplo de chamada à API: `https://api.themoviedb.org/3/movie/popular?api_key=<<api_key>>&page=1`
  * Paginação na tela de lista, com endless scroll / scroll infinito (incrementando o parâmetro `page`).
  * Cada filme popular deve exibir, no mínimo, a imagem do pôster e o título do filme
  * Ao tocar em um item deve levar a tela de detalhes do filme popular
- __Detalhes de um filme popular__. Exemplo de chamada à API: `https://api.themoviedb.org/3/movie/{movie_id}?api_key=<<api_key>>`
  * Deve exibir, no mínimo, a imagem do pôster, os gêneros, resumo, votos (ex: 6.5/10) e um botão que ao ser tocado deve abrir no browser a página do filme popular  

### **A solução DEVE conter** ##
* Sistema de build Gradle
* Mapeamento JSON -> Objeto (GSON / Jackson / Moshi / etc)
* Codificado utilizando preferencialmente a linguagem Kotlin
* Log das requisições
* Material Design

### **Ganha + pontos se conter** ###

* Framework para comunicação com API
* Testes unitários JVM e/ou instrumentados
* Architecture Components (MVVM)
* Injeção de Depêndencia
* Constraint Layout em pelo menos uma tela
* Cache de imagens e da API
* Suportar mudanças de orientação das telas sem perder estado

### **Sugestões** ###

As sugestões de bibliotecas fornecidas são só um guideline, sintam-se a vontade para usar diferentes e nos surpreenderem. O importante de fato é que os objetivos macros sejam atingidos. =)

* Dagger2 | Koin | Kodein
* RxJava/RxKotlin | Coroutines
* Retrofit | Volley | Spring-Android
* Picasso | Universal Image Loader | Glide
* Espresso | Robotium | Robolectric | JUnit

### **OBS** ###

A foto do mockup é meramente ilustrativa, pode usar sua criatividade.  

### **Processo de submissão** ###

O candidato deverá implementar a solução e enviar um pull request para este repositório com a solução.

O processo de Pull Request funciona da seguinte maneira:

1. Candidato fará um fork desse repositório (não irá clonar direto!)
2. Fará seu projeto nesse fork.
3. Commitará e subirá as alterações para o __SEU__ fork.
4. Pela interface do Bitbucket, irá enviar um Pull Request.

Se possível deixar o fork público para facilitar a inspeção do código.

### **ATENÇÃO** ###

Não se deve tentar fazer o PUSH diretamente para ESTE repositório!